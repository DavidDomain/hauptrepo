# HauptRepo

Dies ist ein Beispielprojekt, das aufzeigt, wie ein GitLab-Repository mit einem Submodul funktioniert. Das hier enthaltene `SubRepo` dient als Demonstration des Submodulkonzepts in GitLab.

## Submodule

Das Repository enthält ein Submodul namens `SubRepo`. Dieses Submodul dient als eigenständiges Projekt, das innerhalb dieses Hauptprojekts gepflegt und verwaltet wird.

### Submodul hinzufügen

Wenn Sie das Submodul zu einem bestehenden Repository hinzufügen möchten, führen Sie die folgenden Schritte aus:

1. Klonen Sie Ihr Hauptrepository (falls noch nicht geschehen):  
```
git clone [URL Ihres HauptRepo]
```

2. Navigieren Sie in das Repository-Verzeichnis:  
```
cd HauptRepo
```

3. Fügen Sie das Submodul hinzu:  
```
git submodule add [URL Ihres SubRepo] SubRepo
```

4. Commiten und pushen Sie die Änderungen:  
```
git add .
git commit -m "Submodul hinzugefügt"
git push origin master
```

### Verwendung

Um das Hauptrepository mitsamt dem Submodul zu klonen, verwenden Sie:  
```
git clone --recursive [URL Ihres HauptRepo]
```
